import Vue from "vue";
import {
  SKY_DISTANCE,
  SKY_MOVING_ANIMATION_DUR,
  OCTAHEDRON_RADIUS,
  FUSE_TIMEOUT,
  CURSOR_COLOR,
  OBJ_COLOR,
} from "@/config.json"

const { Tween, Easing } = AFRAME.TWEEN;
const { sin, cos } = Math

const getRadian = angle => angle * (Math.PI / 180)
const getAObjComponent = ({ distance = 3, positionY = 0.5 } = {}) => Vue.extend({
  props: ["angle"],
  computed: {
    radian() {
      return getRadian(+this.angle);
    },
    position() {
      return `${sin(this.radian) * distance} ${positionY} ${-cos(this.radian) * distance}`
    },
    rotation() {
      return `0 ${-this.angle} 0`
    },
  }
})

export default {
  AGoto: getAObjComponent({ distance: 10, positionY: 0.5 }).extend({
    render() {
      return (
        <a-entity class="goto" position={this.position} rotation={this.rotation}>
          <a-octahedron color={OBJ_COLOR} radius={OCTAHEDRON_RADIUS}>
            <a-animation easing="linear" attribute="scale" dur="1000" from="0 0 0" to="1 1 1" />
            <a-animation easing="linear" attribute="rotation" dur="5000" to="0 360 0" repeat="indefinite" />
            <a-animation begin={this.gotoEventStr} attribute="scale" from="1 1 1" to="0 2 0" dur="250" on-animationend={() => this.onGotoClick()} />
          </a-octahedron>
        </a-entity>
      )
    },
    props: ["goto", "vr"],
    methods: {
      onGotoClick() {
        this.$emit("click", this.angle, this.goto)
      }
    },
    computed: {
      gotoEventStr() {
        if (this.vr || !AFRAME.utils.device.isMobile()) {
          return "click"
        }
        else {
          return "mousedown"
        }
      }
    },
  }),
  ATransitionSky: {
    render() {
      return (
        <a-entity class="sky" ref="entityEl">
          <a-sky class="old-city" src={this.oldCity.src} radius={SKY_DISTANCE/2} position="0 0 0" rotation={this.oldCity.rotation}></a-sky>
          {this.newCity.src &&
            <a-sky class="new-city" src={this.newCity.src} radius={SKY_DISTANCE/2} position={this.position} rotation={this.newCity.rotation} ref="newSky"></a-sky>
          }
        </a-entity>
      )
    },
    data: () => ({
      newCity: {
        src: "",
        rotation: "0 0 0",
      },
      oldCity: {
        src: "",
        rotation: "0 0 0"
      },
      camera: 0
    }),
    props: ["src", "rotation"],
    mounted() {
      this.oldCity.src = this.src
      this.oldCity.rotation = this.rotation || "0 0 0"
    },
    computed: {
      radian() {
        return getRadian(this.camera);
      },
      to() {
        return { x: sin(this.radian) * SKY_DISTANCE, y: 0, z: cos(this.radian) * SKY_DISTANCE }
      },
      position() {
        return `${-sin(this.radian) * SKY_DISTANCE} 0 ${-cos(this.radian) * SKY_DISTANCE}`
      }
    },
    methods: {
      onChange({ src, camera, rotation }) {
        const { entityEl } = this.$refs
        this.camera = camera
        const entityPosition = entityEl.getAttribute("position")
        const promise = new Promise(resolve =>
          new Tween(entityPosition)
            .to(this.to, SKY_MOVING_ANIMATION_DUR)
            .easing(Easing.Quintic.InOut)
            .onComplete(resolve)
            .start()
        )

        this.newCity.src = src
        this.newCity.rotation = `0 ${rotation + camera || 0} 0`
        return promise.then(() => {
          this.oldCity.src = src
          this.oldCity.rotation = `0 ${rotation + camera || 0} 0`
          entityPosition.x = 0
          entityPosition.y = 0
          entityPosition.z = 0
          this.newCity.src = ""
        })
      },
    }
  },
  ACursorCamera: {
    template:
      '<a-camera look-controls-enable >' +
      '<a-entity cursor="rayOrigin: mouse"></a-entity>' +
      `<a-cursor v-if="vr" fuse="true" :fuse-timeout="${FUSE_TIMEOUT}" interval="100" raycaster="objects: .goto"` +
      `@click="onCursor('click')" @fusing="onCursor('fusing')" @mouseleave="onCursor('mouseleave')"` +
      `event-set__1="_event: mouseenter; color: ${CURSOR_COLOR};" event-set__2="_event: mouseleave; color: black;">` +
      `<a-ring v-if="isFusingCursor" color="${CURSOR_COLOR}" scale=".1 .1 .1" theta-start="90" material="shader: flat;">` +
      `<a-animation attribute="theta-length" delay="200" from="360" to="0" :dur="${FUSE_TIMEOUT - 200}" />` +
      '</a-ring>' +
      '</a-cursor>' +
      '</a-camera>',
    props: ["vr"],
    data: () => ({
      isFusingCursor: false
    }),
    methods: {
      onCursor(eventName) {
        switch (eventName) {
          case "fusing":
            this.isFusingCursor = true;
            return;
          case "mouseleave":
          case "click":
            this.isFusingCursor = false
            return;
          default:
            console.error(eventName + " switch筒抜け")
            return;
        }
      },
    }
  },
  ATelop: getAObjComponent({ distance: 5, positionY: 3 }).extend({
    render() {
      return (
        <a-entity position={this.position} rotation={this.rotation}>
          <a-image src={`#${this.src}-telop`} width="5.18" height="2.18">
            <a-animation easing="linear" attribute="scale" dur="250" from="1 0 1" to="1 1 1" />
          </a-image>
        </a-entity>
      )
    },
    props: ["src"]
  })
}
